//
//  GameSceneAux.swift
//  sharky-shark
//
//  Created by Arya Irani on 8/14/15.
//  Copyright (c) 2015 Loco Moco Games. All rights reserved.
//

import SpriteKit

class SceneConstants {
    static let sharkyX: CGFloat = 100
    static let deviceToSkyHeightRatio: CGFloat = 0.5
    static let deviceToSeaHeightRatio: CGFloat = 1

    let skyHeight: CGFloat
    let seaHeight: CGFloat
    let worldHeight: CGFloat
    let worldWidth: CGFloat
    let worldSize: CGSize
    let screenHeight: CGFloat

    init(deviceSize: CGSize) {
        skyHeight = SceneConstants.deviceToSkyHeightRatio * deviceSize.height
        seaHeight = SceneConstants.deviceToSeaHeightRatio * deviceSize.height
        worldHeight = [skyHeight, seaHeight].reduce(0, combine: { $0 + $1 })
        worldWidth = deviceSize.width
        worldSize = CGSize(width: worldWidth, height: worldHeight)
        screenHeight = deviceSize.height
    }
}

enum ZPosition: Int {
    // back to front:
    case Sky, Water, Foreground, Frame
}

func setupBackground(layer: SKNode, sceneWidth: CGFloat) {
    func makeOceanNode() -> SKSpriteNode { return SKSpriteNode(imageNamed: "ocean") }

    var water = makeOceanNode()
    var i = 0

    func p() -> CGFloat { return CGFloat(i) * water.size.width }

    let scrollInterval: Double = 3

    while (p() < sceneWidth + water.size.width) {

        water.anchorPoint = CGPoint(x: 0, y: 0.3)
        layer.addChild(water)

        water.position.x = p()

        water.runAction(SKAction.repeatActionForever(
            SKAction.sequence([
                SKAction.moveTo(CGPoint(x: p(), y: 0), duration: 0),
                SKAction.moveBy(CGVector(dx: -water.size.width, dy: 0), duration: scrollInterval)
                ])
            ))

        water.setZ(.Water)

        water = makeOceanNode()
        i++
    }
}

func addMineToLayer(layer: SKNode, sceneWidth: CGFloat, seaHeight: CGFloat) {
    let mine = SKSpriteNode(imageNamed: "mine")

    mine.setZ(.Foreground)
    mine.anchorPoint = CGPoint()
    mine.position.x = sceneWidth + mine.size.width
    mine.position.y = randomRange(min: 0, max: seaHeight-mine.size.height)

    let p = SKPhysicsBody(circleOfRadius: mine.size.width/2)
    mine.physicsBody = p
    p.setCategory(.Mine)
//    p.noCollide() // why does this crash
    p.collisionBitMask = 0
    p.affectedByGravity = false

    mine.runAction(SKAction.sequence([SKAction.moveTo(CGPoint(x: -mine.size.width, y: mine.position.y), duration: 4), SKAction.removeFromParent()]))
    layer.addChild(mine)
}

