//
//  GameScene.swift
//  sharky-shark
//
//  Created by Arya Irani on 8/5/15.
//  Copyright (c) 2015 Loco Moco Games. All rights reserved.
//

import SpriteKit

class GameScene: SKScene, SKPhysicsContactDelegate {

    let sceneConstants: SceneConstants

    let sharky: Sharky

    let world: SKNode

    let screenEdge: SKPhysicsBody

    let backgroundColorNode: SKShapeNode
    let oceanColorNode: SKShapeNode

    override init(size: CGSize) {
        sceneConstants = SceneConstants(deviceSize: size)
        sharky = Sharky(sceneConstants: sceneConstants)

        world = SKNode()

        screenEdge = SKPhysicsBody(edgeLoopFromRect: CGRect(origin: CGPoint(), size: sceneConstants.worldSize))

        backgroundColorNode = SKShapeNode(rectOfSize: CGSize(width: 200, height: 200))
        oceanColorNode = SKShapeNode(
            rect: CGRect(
                origin: CGPoint(),
                size: CGSize(
                    width: sceneConstants.worldWidth,
                    height: sceneConstants.seaHeight
                )
            )
        )

        super.init(size: size)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMoveToView(view: SKView) {
        /* Setup your scene here */
        self.physicsWorld.contactDelegate = self

        self.addChild(world)

        let edge = SKNode()
        edge.physicsBody = screenEdge
        world.addChild(edge)
        describeNode(edge, "edge")
        
        backgroundColorNode.fillColor = SKColor.redColor()
        backgroundColorNode.lineWidth = 0
        backgroundColorNode.setZ(.Sky)

        world.addChild(backgroundColorNode)

        //        setupBackground(self, self.size.width)
        oceanColorNode.fillColor = SKColor.blueColor()
        oceanColorNode.lineWidth = 0
        oceanColorNode.setZ(.Water)
        world.addChild(oceanColorNode)

        world.addChild(sharky.node)
        

        runAction(
            SKAction.repeatActionForever(SKAction.sequence([
                SKAction.runBlock({ addMineToLayer(self.world, self.size.width, self.sceneConstants.seaHeight) }),
                SKAction.waitForDuration(3, withRange: 3)
                ])
            )
        )
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        sharky.lift()
    }
    
    override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
        sharky.fall()
    }
   
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
        sharky.updateEnvironment()
        sharky.applyForces()

        centerViewport()
    }

    func centerViewport() {
        // reverse engineer where the screen center should be, in world coordinates
        let sharkyYInWorld = sharky.node.position.y
        let worldYInViewport = world.position.y
        let sharkyYInViewport = worldYInViewport + sharkyYInWorld

        //        debugPrintln(sharkyYInWorld)
        //        debugPrintln(worldYInViewport)
        //        debugPrintln(sharkyYInViewport)
        //        debugPrintln("---")

        // how much to move world to get sharkyYInViewport into range?
        // if sharky is in the top or bottom half-viewport of the world, then he can approach the edge,
        // otherwise, he should vertically centered in the viewport
        if (sharkyYInWorld >= sceneConstants.worldHeight - sceneConstants.screenHeight/2) {
            world.position.y = sceneConstants.screenHeight - sceneConstants.worldHeight
        }
        else if (sharkyYInWorld <= sceneConstants.screenHeight/2) {
            world.position.y = 0
        }
        else {
            let moveWorldYBy = sharkyYInViewport - sceneConstants.screenHeight/2
            world.position.y -= moveWorldYBy
        }

    }

    func didBeginContact(contact: SKPhysicsContact) {
        if (contact.categoryMatch(.Sharky, b: .Mine)) {

            contact.bodyB.node?.removeFromParent()
            backgroundColorNode.runAction(
                SKAction.sequence([
                    SKAction.runBlock({ self.backgroundColorNode.fillColor = SKColor.yellowColor() }),
                    SKAction.waitForDuration(0.1),
                    SKAction.runBlock({ self.backgroundColorNode.fillColor = SKColor.blackColor() })
                    ])
            )
        }

        func category(c: PhysicsCategory?) -> String {
            if let i = c {
                switch i {
                case .Mine: return "mine"
                case .Sharky: return "sharky"
                case .Edge: return "edge"
                default: return "other"
                }
            } else {
                return "unknown"
            }
        }


        println(category(contact.bodyA.category()) + " collided with " + category(contact.bodyB.category()))
        
    }
}

