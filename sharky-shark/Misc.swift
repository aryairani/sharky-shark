//
//  Misc.swift
//  sharky-shark
//
//  Created by Arya Irani on 8/14/15.
//  Copyright (c) 2015 Loco Moco Games. All rights reserved.
//

import SpriteKit

enum PhysicsCategory: UInt32 {
    case None   = 0
    case All    = 0xFFFFFFFF
    case Mine   = 0x1
    case Edge   = 0x2
    case Sharky = 0x4
}

func randomRange(#min: CGFloat, #max: CGFloat) -> CGFloat {
    assert(min < max)
    return CGFloat(arc4random()) / 0xFFFFFFFF * (max - min) + min
}

extension SKNode {
    func setZ(z: ZPosition) {
        self.zPosition = CGFloat(z.rawValue)
    }
}

extension SKPhysicsContact {
    func categoryBitMasks() -> (UInt32, UInt32) {
        return (bodyA.categoryBitMask, bodyB.categoryBitMask)
    }

    func categoryMatch(a: PhysicsCategory, b: PhysicsCategory) -> Bool {
        return (bodyA.categoryBitMask == a.rawValue)
            && (bodyB.categoryBitMask == b.rawValue)
    }
}

extension SKPhysicsBody {
    func category() -> PhysicsCategory? {
        return PhysicsCategory(rawValue: categoryBitMask)
    }
    func setCategory(c: PhysicsCategory) {
        self.categoryBitMask = c.rawValue
    }
    func setCollisionCategory(c: PhysicsCategory) {
        self.collisionBitMask = c.rawValue
    }
    func noCollide() {
        self.collisionBitMask = 0
    }
    func setContactTestCategory(c: PhysicsCategory) {
        self.contactTestBitMask = c.rawValue
    }
    func setCategories(cs: [PhysicsCategory]) {
        self.categoryBitMask = cs.reduce(0, combine: { $0 | $1.rawValue })
    }
}

func describeShapeNode(n: SKShapeNode, name: String) {
    println(name
        + " pos: (" + n.position.x.description + ", " + n.position.y.description + ")"
        + " z: " + n.zPosition.description
        + " color: " + n.fillColor.description
    )
}

func describeNode(n: SKNode, name: String) {
    println(name
        + " pos: (" + n.position.x.description + ", " + n.position.y.description + ")"
        + " z: " + n.zPosition.description
    )
}

