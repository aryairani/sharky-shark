//
//  Sharky.swift
//  sharky-shark
//
//  Created by Arya Irani on 8/14/15.
//  Copyright (c) 2015 Loco Moco Games. All rights reserved.
//

import SpriteKit

enum SharkyMovement {
    case Rising
    case Falling
}

enum SharkyEnv {
    case Sea
    case Sky
}

class Sharky {
    let node: SKNode
    var movement: SharkyMovement
    var env: SharkyEnv = .Sea

    let seaHeight: CGFloat

//    let updateEnvironment: () -> ()

    init(sceneConstants: SceneConstants, y: CGFloat) {

        let debugNode: SKShapeNode
        let spriteNode: SKSpriteNode


        spriteNode = SKSpriteNode(imageNamed: "shark")
        debugNode = SKShapeNode(rect: CGRect(origin: CGPoint(), size: spriteNode.size))
        node = SKNode()
        node.addChild(debugNode)
        node.addChild(spriteNode)

        node.position = CGPoint(x: SceneConstants.sharkyX, y: y)
//        node.anchorPoint = CGPoint(x: 0.0, y: 0.7)
        spriteNode.anchorPoint = CGPoint(x: 0.0, y: 0.0)

        node.setZ(.Foreground)

        node.physicsBody = SKPhysicsBody(rectangleOfSize: spriteNode.size)
        node.physicsBody!.allowsRotation = false
        node.physicsBody!.setCategory(.Sharky)
        node.physicsBody!.setCollisionCategory(.Mine)
        node.physicsBody!.setContactTestCategory(.Mine)

        movement = .Falling

        seaHeight = sceneConstants.seaHeight
        updateEnvironment()
    }


    func updateEnvironment() {
        if self.node.position.y > self.seaHeight {
            self.env = .Sky
        } else {
            self.env = .Sea
        }
    }


    convenience init(sceneConstants: SceneConstants) {
        self.init(sceneConstants: sceneConstants, y: sceneConstants.seaHeight/2)
    }

    func lift() {
        movement = .Rising
        node.zRotation = CGFloat(M_PI_4/4)
    }

    func fall() {
        movement = .Falling
        node.zRotation = 0
    }

    func applyForces() {
        switch env {
        case .Sea:
            node.physicsBody!.linearDamping = 15
            if movement == .Rising {
                node.physicsBody!.applyForce(CGVector(dx: 0.0, dy: 1500.0))
            }
        case .Sky:
            node.physicsBody!.linearDamping = 5
        }
    }
}